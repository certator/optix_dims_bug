
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>
#include <optix.h>

#include <sched.h>


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#if defined(__APPLE__)
        #  include <GLUT/glut.h>
        #  define GL_FRAMEBUFFER_SRGB_EXT           0x8DB9
        #  define GL_FRAMEBUFFER_SRGB_CAPABLE_EXT   0x8DBA
#else
        #include <GL/freeglut.h>
#endif




using namespace optix;

class RenderProcess {
public:
    Context      _context;

    Buffer       output_buffer_obj;

    RenderProcess()
    {}


    void createContext(void);

    void createRaytrace(void);

    const char* const ptxpath(const std::string& base )
    {
        const std::string   target_name = "optix_test";
        static std::string path;
        path = std::string("lib/ptx") + "/" + target_name + "_generated_" + base + ".ptx";
        return path.c_str();
    }


    void setup_scene(void)
    {
        createContext();

        createRaytrace();

    }

    void render_prepare(void)
    {


    	_context->validate();
        _context->compile();

        render_frame();


    }

    void render_frame(void)
    {


    	// ATTENTION: B+C+D - works; C+D - not works


//		_context->launch(0, 512, 512); // B

		_context->launch(0, 64, 64); // C

		_context->launch(0, 512, 512); // D

		std::cout << "frame rendered" << std::endl;


    }


    RTresult initGlut(int* argc, char** argv)
    {
        // Initialize GLUT
        glutInit(argc, argv);
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
        glutInitWindowSize(100, 100);
        glutInitWindowPosition(100, 100);
        glutCreateWindow(argv[0]);

        return RT_SUCCESS;
    }


};


int main(int argc, char* argv[])
{



    RenderProcess render;

    render.initGlut(&argc, argv);

    render.setup_scene();

    render.render_prepare();
    render._context->destroy();
    return( 0 );
}

void RenderProcess::createContext(void)
{

    _context = optix::Context::create();

    _context->setPrintEnabled(true);

    /* Render result buffer */
    output_buffer_obj = _context->createBuffer(RT_BUFFER_OUTPUT);
    output_buffer_obj->setFormat(RT_FORMAT_UNSIGNED_BYTE4);
    output_buffer_obj->setSize(512, 512);
    _context["output_buffer"]->setBuffer(output_buffer_obj);

}


void RenderProcess::createRaytrace(void)
{

    _context->setRayTypeCount( 2 );
    _context->setEntryPointCount( 1 );
    _context->setStackSize( 2600 );

    _context->setPrintEnabled(true);
    _context->setExceptionEnabled(RT_EXCEPTION_ALL, true);


    std::string         ptx_path = ptxpath( "test_write.cu" );

    _context->setRayGenerationProgram(0, _context->createProgramFromPTXFile( ptx_path, "test_write" ) );

    _context->setExceptionProgram(0, _context->createProgramFromPTXFile( ptx_path, "exception" ) );


}

