
#include <optix.h>
#include <optixu/optixu_math_namespace.h>

#include <optix_device.h>


using namespace optix;


rtBuffer<uchar4, 2>              output_buffer;

rtDeclareVariable(uint2,      launch_index, rtLaunchIndex, );



RT_PROGRAM void test_write()
{
    output_buffer[launch_index] = make_uchar4(40, 0, 40, 0);
}



RT_PROGRAM void exception()
{
  const unsigned int code = rtGetExceptionCode();
  if (launch_index.x == 16 && launch_index.y == 8)
  {
		rtPrintf( "Caught exception 0x%X at launch index (%d,%d)\n", code, launch_index.x, launch_index.y );
		rtPrintExceptionDetails();
  }
}
