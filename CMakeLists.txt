cmake_minimum_required(VERSION 2.8.2 FATAL_ERROR)

# As of CMake 2.6 policies were introduced in order to provide a mechanism for
# adding backwards compatibility one feature at a time.  We will just specify
# that all policies will use version 2.6 symantics.
cmake_policy(VERSION 2.6)

set(CMAKE_MODULE_PATH
  "${CMAKE_CURRENT_SOURCE_DIR}"
  ${CMAKE_MODULE_PATH}
  )


set(CMAKE_VERBOSE_MAKEFILE "ON")

project(optix_test)

include(optix)

include_directories(${GLUT_INCLUDE_DIR})
add_definitions(-DGLUT_FOUND -DGLUT_NO_LIB_PRAGMA)

set(CMAKE_VERBOSE_MAKEFILE "ON")


OPTIX_add_executable( ${PROJECT_NAME}
  test_write.cpp
  
  test_write.cu

  )
  
  
#include(../cmake/basic.txt)
  
